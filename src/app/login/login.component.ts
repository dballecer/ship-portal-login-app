import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService,  } from '../_services/auth.service';
import { AlertService } from '../_services/alert.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  @ViewChild('loginForm', {static: false} ) loginForm: NgForm;

  submitted = false;
  loading = false;
  errorFound = false;

  constructor( private authService: AuthService,
               private router: Router,
               private alertService: AlertService ) { }

  ngOnInit() : void {
    
  }

  onSubmit( loginForm: NgForm ) {
    this.loading = false;
    this.submitted = true;

    if( this.loginForm.invalid ) {
      return;
    }
    
  
    this.authService.login( {userName: loginForm.value.username, password: loginForm.value.password }  )
      .subscribe(
        response => {
          this.router.navigate(['/']);
        },
        error => {
          let errorObj = error['error'];
          let errMsg = '';
          if( 404 == errorObj['status'] ) {
            errMsg = ' : User not found';
          } 

          this.alertService.error( 'Unable to login with error' + errMsg );
        }
      );
      loginForm.reset();   
  }
}
