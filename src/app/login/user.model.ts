export class UserModel {

    public userName: string;
    public firstName: string;
    public middleName: string;
    public lastName: string;
    public emailAddress: string;
    public displayName: string;

    constructor( userName: string,
        firstName: string,
        middleName: string,
        lastName: string,
        emailAddress: string,
        displayName: string ) {

        this.userName = userName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.emailAddress = emailAddress;
        this.displayName = displayName;
    }
}