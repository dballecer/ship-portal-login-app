import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { AuthGuardService } from './_services/auth-guard.service';
import { AuthService } from './_services/auth.service';
import { AlertComponent } from './_directives/alert/alert.component';
import { AlertService } from './_services/alert.service';

const appRoutes: Routes =[
  { 
    path: '',
    canActivate: [ AuthGuardService ], 
    component: HomeComponent 
  },
  {
    path: 'auth',
    data: { title: 'Please Sign In'},
    component: LoginComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    AlertComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [ AuthGuardService, AuthService, AlertService ],
  bootstrap: [AppComponent]
})

export class AppModule { }