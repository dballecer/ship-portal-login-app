import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { Router } from '@angular/router';
import { UserModel } from '../login/user.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {

  userInfo: UserModel;
  private authUserSub: Subscription;

  constructor( private authService: AuthService,
               private router: Router ) { }

  ngOnInit(): void {

    this.authUserSub = this.authService.getAuthenticatedUser().subscribe(
      result => {
        this.userInfo = result;
      }
    );
  }

  ngOnDestroy() {
    this.authUserSub.unsubscribe();
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/auth']);
  }

}
