import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { map, take } from 'rxjs/operators';


@Injectable()
export class AuthGuardService implements CanActivate {

    constructor( private authService: AuthService,
                 private router: Router ) {}

    canActivate( route: ActivatedRouteSnapshot ,
                 state: RouterStateSnapshot) : Observable<boolean | UrlTree> | Promise<boolean | UrlTree > | boolean | UrlTree 
                 {
                    return this.authService.getAuthenticatedUser().pipe(
                        map( user => {
                            const isAuth = !!user;
                            if (isAuth) {
                                return true;
                              }
                              return this.router.createUrlTree(['/auth']);
                        })
                    )
                 }
}