import { Subject } from 'rxjs';

export class AlertService
{
    private alertMessage = new Subject<any>();

    info( message: string ) {
        this.alertMessage.next({
            type: 'info',
            title: '',
            message: message
        })
    }
    
    
    success( message: string ) {
        this.alertMessage.next({
            type: 'success',
            title: 'Success',
            message: message
        })
    }

    error( message: string ) {
        this.alertMessage.next({
            type: 'danger',
            title: 'Error',
            message: message
        })
    }

    getAlertMessage() {
        return this.alertMessage.asObservable();
    }
}