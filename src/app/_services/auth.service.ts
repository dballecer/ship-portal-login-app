import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { UserModel } from '../login/user.model';

@Injectable()
export class AuthService {
    loggedIn = false;
    authenticatedUser = new BehaviorSubject<UserModel>(null);

    constructor( private http: HttpClient ) {}

    login( credentials: {userName: string, password: string } ) {
        return this.http.post<Response>('http://localhost:8080/authenticate', credentials )
            .pipe(
                tap( 
                    ( res ) => {
                        this.setUser( res['principal'] );
                    }
                )
            );
    }

    logout() {
        this.loggedIn = false;
        localStorage.removeItem('userData');
        this.authenticatedUser.next(null);
    }

    getAuthenticatedUser() {
        const userData: UserModel = JSON.parse(localStorage.getItem('userData'));
        this.authenticatedUser.next( userData );
        return this.authenticatedUser;
    }

    private setUser( user: UserModel ) {
        localStorage.setItem('userData',  JSON.stringify(user) );
        this.authenticatedUser.next(user);
        this.loggedIn = true;
    }
}