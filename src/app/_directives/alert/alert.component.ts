import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { AlertService } from 'src/app/_services/alert.service';

@Component({
    selector: 'alert',
    templateUrl: './alert.component.html',
    styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit, OnDestroy
{
    alertSubscription = new Subscription;
    alertMessage: any;

    constructor( private alertService: AlertService ) {}
    
    ngOnInit() {

        this.alertSubscription = this.alertService.getAlertMessage()
            .subscribe(
                ( alert ) => {
                    this.alertMessage = alert;
                }
            );   
    }

    ngOnDestroy() {
        this.alertSubscription.unsubscribe();
    }
}